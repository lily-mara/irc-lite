use actix_web::{middleware, web, App, HttpResponse, HttpServer};
use futures::{Future, Stream, future};
use rand::distributions::Distribution;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::{sync::RwLock, time, mem};

mod future_adapter;

struct Data {
    users: RwLock<HashMap<String, String>>,
    message_bus: RwLock<bus::Bus<Message>>,
}

#[derive(Debug, Clone, Serialize)]
struct Message {
    sender: String,
    message: String,
}

impl Data {
    fn new() -> Self {
        Data {
            users: RwLock::new(HashMap::new()),
            message_bus: RwLock::new(bus::Bus::new(100)),
        }
    }
}

#[derive(Debug, Deserialize)]
struct RegisterRequest {
    username: String,
}

#[derive(Debug, Serialize)]
struct RegisterResponse {
    username: String,
    token: String,
}

#[derive(Debug, Deserialize)]
struct MessageRequest {
    username: String,
    token: String,
    message: String,
}

fn get_messages(state: web::Data<Data>) -> Box<dyn Future<Item = HttpResponse, Error = ()>> {
    let rx = {
        let mut lock = state.message_bus.write().unwrap();
        lock.add_rx()
    };

    let stream = future_adapter::recv_future(rx);

    let start = time::Instant::now();
    let mut count = -1;

    Box::new(
        stream
            .take_while(move |_| {
                count+= 1;

                if count == 0 {
                    return future::ok(true);
                }

                let now = time::Instant::now();
                let elapsed = now.duration_since(start.clone()).as_millis();

                future::ok(elapsed < 500)
            })
            .collect()
            .map(|msgs| HttpResponse::Ok().json(msgs)),
    )
}

fn register(state: web::Data<Data>, req: web::Json<RegisterRequest>) -> HttpResponse {
    let username = &req.username;
    let mut w = state.users.write().unwrap();
    if w.contains_key(username) {
        return HttpResponse::Conflict().body("username exists");
    }

    let rng = rand::thread_rng();

    let token: String = rand::distributions::Alphanumeric
        .sample_iter(rng)
        .take(20)
        .collect();
    w.insert(username.clone(), token.clone());

    let response = RegisterResponse {
        username: username.clone(),
        token,
    };

    HttpResponse::Ok().json(response)
}

fn send_message(state: web::Data<Data>, req: web::Json<MessageRequest>) -> HttpResponse {
    {
        let lock = state.users.read().unwrap();

        match lock.get(&req.username) {
            None => return HttpResponse::Unauthorized().body("no such username"),
            Some(x) if *x != req.token => return HttpResponse::Unauthorized().body("bad token"),
            _ => {}
        }
    }

    {
        let mut lock = state.message_bus.write().unwrap();
        lock.broadcast(Message {
            sender: req.username.clone(),
            message: req.message.clone(),
        });
    }

    HttpResponse::Ok().body("ok")
}

fn main() -> std::io::Result<()> {
    let state = web::Data::new(Data::new());

    HttpServer::new(move || {
        App::new()
            .register_data(state.clone())
            .wrap(middleware::Logger::default())
            .route("/register", web::post().to(register))
            .route("/send-message", web::post().to(send_message))
            .route("/messages", web::get().to(get_messages))
    })
    .bind("127.0.0.1:8088")
    .unwrap()
    .run()
}
